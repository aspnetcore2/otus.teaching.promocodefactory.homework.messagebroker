﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.ModelOperationsResult
{
    public class EmplServiceOperationResult
    {
        public bool IsEmplFound { get; set; }

        public bool Ok { get; set; }
    }
}
