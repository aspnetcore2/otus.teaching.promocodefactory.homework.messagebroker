﻿
namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.RabbitMQ.Configuration
{
    public class RabbitMqConfig
    {
        public string Host { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string QueueName { get; set; }

    }
}
